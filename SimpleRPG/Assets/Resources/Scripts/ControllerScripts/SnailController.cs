﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailController : MonoBehaviour {

    public GameObject player;
    public float speed = 1;

    private float height;

	// Use this for initialization
	void Start () {
        //transform.rotation = new Quaternion(0,270,0,1);
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");

        height = this.transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {

        if(Vector3.Distance(player.transform.position, transform.position) < 10.0f && Vector3.Distance(player.transform.position, transform.position) > 2.0f)
        {
            Vector3 direction = (player.transform.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

            this.transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, 0.01f * speed);
            this.transform.position = new Vector3(this.transform.position.x, height, this.transform.position.z);
        }
	}
}
