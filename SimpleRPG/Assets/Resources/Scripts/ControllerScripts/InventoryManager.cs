﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class InventoryManager : MonoBehaviour {
    
    #region API for key bindings

    const string DLL_NAME = "UnityPlugins";

    [DllImport(DLL_NAME)]
    private static extern void SendEvent(System.IntPtr data, int type);

    [DllImport(DLL_NAME)]
    private static extern System.IntPtr AddItem(string name, int amount);

    [DllImport(DLL_NAME)]
    private static extern System.IntPtr GetItemName(System.IntPtr item);

    [DllImport(DLL_NAME)]
    private static extern int GetItemAmount(System.IntPtr item);

    [DllImport(DLL_NAME)]
    private static extern void SetItemAmount(System.IntPtr item, int amount);

    [DllImport(DLL_NAME)]
    private static extern void DeleteItem(System.IntPtr ptr);

    /// <summary>
    /// Proxy class for our memory managed C++ items
    /// </summary>
    private class Item
    {
        /// <summary>
        /// Creates a new item, with the given name and amount
        /// </summary>
        /// <param name="name">The name of the item</param>
        /// <param name="amount">The amount to associate with the item</param>
        public Item(string name, int amount) {
            // Call the c++ method to allocate from pool and store the pointer
            myPtr = AddItem(name, amount);
        }
        /// <summary>
        /// Deletes this item, and frees the underlying pointer
        /// </summary>
        ~Item() { DeleteItem(myPtr); }

        /// <summary>
        /// Gets or sets the amount associated with this item
        /// </summary>
        public int Amount
        {
            // These simply proxy to the c++ side
            get { return GetItemAmount(myPtr); }
            set { SetItemAmount(myPtr, value); }
        }
        /// <summary>
        /// Gets the name of this item
        /// </summary>
        public string Name
        {
            // We have to convert the raw pointer to a CLR string
            get { return Marshal.PtrToStringAnsi(GetItemName(myPtr)); }
        }

        /// <summary>
        /// Stores the underlying pointer from our unmanaged code
        /// </summary>
        private System.IntPtr myPtr;

        /// <summary>
        /// Implicitly converts an item to it's underlying pointer
        /// </summary>
        /// <param name="item">The item to convert</param>
        public static implicit operator System.IntPtr(Item item) { return item.myPtr; }
    }

    #endregion

    // Used for storing the inventory items
    Dictionary<string, Item> items;

    // Get references to GameObjects
    public GameObject InventorySlot;
    public GameObject content;
    public GameObject player;

	// Use this for initialization
	void Start () {
        // Look for the player object if not given
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");

        // Initialize the dictionary for the items
        items = new Dictionary<string, Item>();        
    }
	
    public void ToggleInventory()
    {
        if (!player.GetComponent<CharacterController>().isCanvasActive)
        {
            player.GetComponent<CharacterController>().ToggleInventory();
        }
    }

    public void AddToInventory(GameObject other)
    {
        if (items.ContainsKey(other.gameObject.name)) {
            items[other.gameObject.name].Amount++;
            SendEvent(items[other.gameObject.name], (int)Type.InventoryUpdated);
        } else {
            Item item = new Item(other.gameObject.name, 1);

            items.Add(other.gameObject.name, item);

            GameObject slot = Instantiate(InventorySlot, content.transform, false);

            // Name the Inventory Slot
            slot.name = item.Name + "_Slot";

            // Set the image child to the image sprite
            slot.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Items/" + item.Name);

            // Set the text to the item count
            slot.transform.GetChild(1).GetComponent<Text>().text = item.Amount.ToString();
        }

        // Update the inventory display
        if (player.GetComponent<CharacterController>().isInventoryActive)
        {
            UpdateList();
        }
    }

    public float GetItemCount(string name) {
        if (!items.ContainsKey(name))
            return 0;
        else
            return GetItemAmount(items[name]);
    }

    public int RemoveItem(string name, int count, bool acceptLess = true) {
        int result = count;
        if (items.ContainsKey(name)) {
            if (items[name].Amount >= count) {
                items[name].Amount -= count;
                result = 0;
            } else if (acceptLess) {
                result = count - items[name].Amount;
                items[name].Amount = 0;
            }

            if (items[name].Amount <= 0) {
                Destroy(content.transform.Find(name + "_Slot").gameObject);
                items.Remove(name);
            }
        }
        return result;
    }

    public void UpdateList()
    {
        // Update the values for the inventory items
        foreach (KeyValuePair<string, Item> item in items) {
            GameObject panel = GameObject.Find(item.Value.Name + "_Slot");
            if (panel != null)
                panel.transform.GetChild(1).GetComponent<Text>().text = item.Value.Amount.ToString();
        }
    }
}
