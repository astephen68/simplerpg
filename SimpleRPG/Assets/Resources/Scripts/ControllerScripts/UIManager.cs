﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
public class UIManager : MonoBehaviour {

    #region API for key bindings

    const string DLL_NAME = "UnityPlugins";

    [DllImport(DLL_NAME)]
    private static extern void Init();
    [DllImport(DLL_NAME)]
    private static extern void Cleanup();
    [DllImport(DLL_NAME)]
    private static extern void ChangeKeyBinding(string key, int value);
    [DllImport(DLL_NAME)]
    private static extern void ResetKey(string key);
    [DllImport(DLL_NAME)]
    private static extern int GetKey(string key);

    [DllImport(DLL_NAME)]
    private static extern System.IntPtr GetKeyName(int pos);
    [DllImport(DLL_NAME)]
    private static extern void ResetAllKeyBindings();
    [DllImport(DLL_NAME)]
    private static extern int GetTotalAmountOfKey();
    [DllImport(DLL_NAME)]

    private static extern int GetDefaultKeyValue(string value);

    [DllImport(DLL_NAME)]

    private static extern void Undo();

    [DllImport(DLL_NAME)]

    private static extern void Redo();
    #endregion

    List<GameObject> keyList;
    public List<string> textFieldValues;

    public RectTransform content;
    public Text warningText;
    public GameObject UIControlObject;
    public GameObject player;

    Event keyEvent;
    KeyCode newKey;
    bool isRecording;
    public InputField selectedField;

    public static bool isOpened = true;

    public void ToggleMenu()
    {
        if (player.GetComponent<CharacterController>().isCanvasActive)
        {
            player.GetComponent<CharacterController>().ToggleMenu();
            isOpened = !isOpened;
        }
    }

    void CreateList()
    {
        // Create a list to store all of the key bindings
        keyList = new List<GameObject>();
        textFieldValues = new List<string>();

        // Create UI Controls for each key binding to add to the content panel
        for (int i = 0; i < GetTotalAmountOfKey(); i++)
        {
            GameObject controlObject = Instantiate(UIControlObject, content.transform, false);

            // Name the UI control
            controlObject.name = Marshal.PtrToStringAnsi(GetKeyName(i));

            // Name the action text and display action text
            controlObject.transform.GetChild(0).name = Marshal.PtrToStringAnsi(GetKeyName(i)) + "Text";
            controlObject.transform.GetChild(0).GetComponent<Text>().text = Marshal.PtrToStringAnsi(GetKeyName(i)) + " Key:";

            // Name the key binding text and display the current key
            controlObject.transform.GetChild(1).name = Marshal.PtrToStringAnsi(GetKeyName(i)) + "Key";
            KeyCode code = new KeyCode();
            code = (KeyCode)(GetKey(Marshal.PtrToStringAnsi(GetKeyName(i))));
            controlObject.transform.GetChild(1).GetComponent<Text>().text = code.ToString();

            // Name the input field
            controlObject.transform.GetChild(2).name = Marshal.PtrToStringAnsi(GetKeyName(i)) + "InputField";
            // Name the button
            controlObject.transform.GetChild(3).name = Marshal.PtrToStringAnsi(GetKeyName(i)) + "Button";
            // Debug.Log(Marshal.PtrToStringAnsi(GetKeyName(i)).ToString());
            string key = Marshal.PtrToStringAnsi(GetKeyName(i)).ToString();
            controlObject.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(
                delegate {
                    ResetKeyEvent(key);
                    }
                );

            // Add the new UI control to the list
            keyList.Add(controlObject);

        }
    }

    // Use this for initialization
    void Start()
    {
        //Init();
        warningText.gameObject.SetActive(false);

        isRecording = false;

        CreateList();

        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        // This block is needed to prevent the input fields from displaying "None" when holding down a letter key
        if (isRecording)
        {
            foreach (GameObject item in keyList)
            {
                if (item.transform.GetChild(2).GetComponent<InputField>().text == "None")
                {
                    // Change the "None" text to empty space
                    item.transform.GetChild(2).GetComponent<InputField>().text = "";
                }
            }
        }

        // When the left mouse button is clicked
        if (Input.GetMouseButtonUp(0))
        {
            foreach (GameObject item in keyList)
            {
                // Check each input field to see if it is currently selected
                if (item.transform.GetChild(2).GetComponent<InputField>().isFocused)
                {
                    // Hide the warnimg message
                    warningText.gameObject.SetActive(false);

                    // If an input field is selected, set it as our active input field
                    selectedField = item.transform.GetChild(2).GetComponent<InputField>();
                    break;
                }
            }
        }

        /*if(Input.GetKeyDown(KeyCode.Escape) && this.gameObject.active && selectedField == null)
        {
            print("This should close");
        }*/
    }

    void OnGUI()
    {
        /* keyEvent dictates what key our user presses bt using Event.current to detect the current event */
        keyEvent = Event.current;

        //Executes if the user presses a key
        if (keyEvent.isKey)
        {
            isRecording = true; // When true, we are waiting for input
            newKey = keyEvent.keyCode; //Assigns newKey to the key user presses  

            foreach (GameObject item in keyList)
            {
                if (selectedField != null)
                {
                    // Check if our currently selected field is the same as our active input field
                    if (item.transform.GetChild(2).GetComponent<InputField>().name == selectedField.name)
                    {
                        // Update the input field to display the users information
                        item.transform.GetChild(2).GetComponent<InputField>().text = newKey.ToString();
                        break;
                    }
                }
            }
            //selectedField = null;
        }
        else
        {
            isRecording = false; // When false, we are not waiting for input
        }
    }

    public void ResetKeyEvent(string key)
    {
        Debug.Log(GetDefaultKeyValue(key).ToString());
        textFieldValues.Clear();

        foreach (GameObject item in keyList)
        {
            for (int i = 0; i < GetTotalAmountOfKey(); i++)
            {
                // Compare the input field to all values already bound to keys
                if (((KeyCode)GetKey(Marshal.PtrToStringAnsi(GetKeyName(i)))) == (KeyCode)GetDefaultKeyValue(key))
                {
                    // If the key is already bound, display an erro message and prevent the user from making changes
                    warningText.text = "One or more keys are already being used!";
                    warningText.gameObject.SetActive(true);
                    return;
                }
            }
        }


        ResetKey(key);
        foreach (GameObject item in keyList)
        {
            // Update the UI components
            item.transform.GetChild(1).GetComponent<Text>().text = ((KeyCode)GetKey(item.name)).ToString();
            item.transform.GetChild(2).GetComponent<InputField>().text = "";
        }
    }
    
    public void RestoreDefault()
    {
        //reset the bindings total
        ResetAllKeyBindings();
        // Hide the warning text
        warningText.gameObject.SetActive(false);
        foreach (GameObject item in keyList)
        {
            // Update the UI components
            item.transform.GetChild(1).GetComponent<Text>().text = ((KeyCode)GetKey(item.name)).ToString();
            item.transform.GetChild(2).GetComponent<InputField>().text = "";
        }
        selectedField = null;
    }

    public void ApplyChanges()
    {
        textFieldValues.Clear();

        foreach (GameObject item in keyList)
        {
            // Check if any for our input fields contain the same information
            if (textFieldValues.Contains(item.transform.GetChild(2).GetComponent<InputField>().text) && item.transform.GetChild(2).GetComponent<InputField>().text != "")
            {
                textFieldValues.Clear();
                // Display a warning and prevent the user from making changes
                warningText.text = "Two or more fields contain the same information!";
                warningText.gameObject.SetActive(true);
                return;
            }
            else if (!textFieldValues.Contains(item.transform.GetChild(2).GetComponent<InputField>().text))
            {
                // If the other input fields don't contain this information, add it to the list
                textFieldValues.Add(item.transform.GetChild(2).GetComponent<InputField>().text);
            }
        }
                
        textFieldValues.Clear();

        foreach (GameObject item in keyList)
        {
            // If input field isn't blank
            if (item.transform.GetChild(2).GetComponent<InputField>().text != "")
            {
                for(int i=0; i<GetTotalAmountOfKey(); i++)
                {
                    // Compare the input field to all values already bound to keys
                    if (((KeyCode)GetKey(Marshal.PtrToStringAnsi(GetKeyName(i)))).ToString() == item.transform.GetChild(2).GetComponent<InputField>().text)
                    {
                        // If the key is already bound, display an erro message and prevent the user from making changes
                        warningText.text = "One or more keys are already being used!";
                        warningText.gameObject.SetActive(true);
                        return;
                    }
                }

                // If the key is not bound, update the old key binding to the new key
                //apply changes
                ChangeKeyBinding(item.name, (int)(KeyCode)System.Enum.Parse(typeof(KeyCode), item.transform.GetChild(2).GetComponent<InputField>().text));
            }
        }


        foreach (GameObject item in keyList)
        {
            // Update the UI control information
            item.transform.GetChild(1).GetComponent<Text>().text = ((KeyCode)GetKey(item.name)).ToString();
            item.transform.GetChild(2).GetComponent<InputField>().text = "";
        }
        selectedField = null;
    }

    public void UndoEvent()
    {
        Undo();
        foreach (GameObject item in keyList)
        {
            // Update the UI control information
            item.transform.GetChild(1).GetComponent<Text>().text = ((KeyCode)GetKey(item.name)).ToString();
            item.transform.GetChild(2).GetComponent<InputField>().text = "";
        }
    }

    public void RedoEvent()
    {
        Redo();
        foreach (GameObject item in keyList)
        {
            // Update the UI control information
            item.transform.GetChild(1).GetComponent<Text>().text = ((KeyCode)GetKey(item.name)).ToString();
            item.transform.GetChild(2).GetComponent<InputField>().text = "";
        }
    }
    private void OnApplicationQuit()
    {
        Cleanup();
    }
}
