﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public enum Type
{
    AchievementUpdate,
    InventoryUpdated,
    DialogueExecute,
    ScoreUpdate
};

public enum AchivementType
{
    Collection,
    Movement,

};

public class CharacterController : MonoBehaviour {

    #region API for key bindings

    const string DLL_NAME = "UnityPlugins";
    [DllImport(DLL_NAME)]
    private static extern void SetChangeKey(string name);
    [DllImport(DLL_NAME)]
    private static extern void SetNewKey(int key);
    [DllImport(DLL_NAME)]
    private static extern void ChangeKeyBinding();
    [DllImport(DLL_NAME)]
    private static extern int GetKey(string key);
    [DllImport(DLL_NAME)]
    private static extern void ResetAllKeyBindings();

    [DllImport(DLL_NAME)]
    private static extern System.IntPtr GetKeyName(int pos);
    [DllImport(DLL_NAME)]
    private static extern int GetTotalAmountOfKey();

    [DllImport(DLL_NAME)]
    private static extern void Init();

    [DllImport(DLL_NAME)]
    private static extern void Cleanup();

    [DllImport(DLL_NAME)]
     private static extern System.IntPtr CreateAchievement(string description, int type);
    [DllImport(DLL_NAME)]

    private static extern System.IntPtr CreateAchievementAmount(string description, int type, int amount);
    [DllImport(DLL_NAME)]

    private static extern System.IntPtr GetAchivementInfo(System.IntPtr achivement);
    [DllImport(DLL_NAME)]

    private static extern void SendEvent(System.IntPtr data, int type);

    [DllImport(DLL_NAME)]
    private static extern bool isAchivementUnlocked(System.IntPtr data);

    [DllImport(DLL_NAME)]
    private static extern int GetScore();

    [DllImport(DLL_NAME)]
    private static extern System.IntPtr AddScore(int score);

    [DllImport(DLL_NAME)]
    private static extern System.IntPtr GetDialogue();
    [DllImport(DLL_NAME)]
    private static extern System.IntPtr CreateDialogue(string filename);
    #endregion

    #region MyProperties
    public GameObject canvas;
    public GameObject logs;

    public float walkSpeed = 10.0f;
    public float turnSpeed = 100.0f;

    public Vector3 jump;
    public float jumpForce = 2.0f;

    public bool isGrounded;
    public bool isCanvasActive = false;
    public bool isInventoryActive = false;

    //public GameObject myPlayer;
    public GameObject achivementUI;
    public GameObject scoreUI;
    public GameObject interactText;

    Rigidbody rb;
    AchievementManager myAchievements;
    ScoreManager myScore;
    //ScoreManager myScore;
    float currentTime = 0f;

    private bool firstMove = false;
    #endregion
    // Use this for initialization
    void Start () {
        Init();
        myAchievements = GameObject.Find("AchivementManager").GetComponent<AchievementManager>();
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 2.0f, 0.0f);

        if (canvas == null)
        {
            canvas = GameObject.FindGameObjectWithTag("Canvas");
        }

        canvas.gameObject.transform.GetChild(2).gameObject.SetActive(false);
        //create achivements
        myAchievements.addAchievement(
            CreateAchievement("Achievement Unlocked:\n You Learned to Move!", 
            (int)AchivementType.Movement), "Move");

        myAchievements.addAchievement(
             CreateAchievementAmount("Achievement Unlocked:\n Picked up 5 logs!",
                 (int)AchivementType.Collection,5), "Log5");

        myAchievements.addAchievement(
            CreateAchievementAmount("Achievement Unlocked:\n Picked up 10 logs!",
          (int)AchivementType.Collection,10), "Log10");

        myAchievements.addAchievement(
        CreateAchievementAmount("Achievement Unlocked:\n Picked up 20 logs!",
                (int)AchivementType.Collection,20), "Log20");
    }

    public void ToggleMenu()
    {
        isCanvasActive = !isCanvasActive;

        if(isCanvasActive)
        {
            ToggleInventory();
            GameObject.Find("DialogueManager").GetComponent<DialogueManager>().animator.SetBool("IsOpen", false);
            canvas.gameObject.transform.GetChild(2).gameObject.SetActive(true);
            //Time.timeScale = 0.0f;
        }
        else
        {
            canvas.gameObject.transform.GetChild(2).gameObject.SetActive(false);
            Time.timeScale = 1.0f;
        }

        if (GameObject.Find("DialogueManager").GetComponent<DialogueManager>().dialogueEnd == false && isCanvasActive == false)
        {
            GameObject.Find("DialogueManager").GetComponent<DialogueManager>().animator.SetBool("IsOpen", true);
        }
    }

    public void ToggleInventory()
    {
        if (isCanvasActive)
        {
            canvas.gameObject.transform.GetChild(4).gameObject.SetActive(false);
            isInventoryActive = false;
            return;
        }

        if (isInventoryActive)
        {
            if (GameObject.Find("DialogueManager").GetComponent<DialogueManager>().dialogueEnd == false && isInventoryActive == true)
            {
                GameObject.Find("DialogueManager").GetComponent<DialogueManager>().animator.SetBool("IsOpen", true);
            }

            canvas.gameObject.transform.GetChild(4).gameObject.SetActive(false);
        }
        else
        {
            canvas.gameObject.transform.GetChild(4).gameObject.SetActive(true);
            GameObject.FindGameObjectWithTag("InventoryManager").GetComponent<InventoryManager>().UpdateList();

            GameObject.Find("DialogueManager").GetComponent<DialogueManager>().animator.SetBool("IsOpen", false);
        }

        


        isInventoryActive = !isInventoryActive;
    }

    void InputDataWithAchivement(ref float x, ref float z, string direction, int keycode, string achivementName)
    {
        if (Input.GetKey((KeyCode)keycode))
        {
            if (direction == "Left")
            {
                x = -1 * Time.deltaTime * turnSpeed;
            }
            else if (direction == "Right")
            {
                x = 1 * Time.deltaTime * turnSpeed;
            }
            else if (direction=="Down")
            {
                z = -1 * Time.deltaTime * walkSpeed;
            }
            else if (direction=="Up")
            {
                z = 1 * Time.deltaTime * walkSpeed;
            }
            if (myAchievements.GetAchivement(achivementName) != System.IntPtr.Zero &&
                !isAchivementUnlocked(myAchievements.GetAchivement(achivementName)))
            {
                SendEvent(myAchievements.GetAchivement(achivementName), (int)Type.AchievementUpdate);

            }
            if (firstMove == false)
            {
                firstMove = true;
                currentTime = 3.0f;
            }
        }
    }
	// Update is called once per frame
	void Update () {

        float x = 0, z = 0;
        InputDataWithAchivement(ref x, ref z, "Left", GetKey("Left"), "Move");
        InputDataWithAchivement(ref x, ref z, "Right", GetKey("Right"), "Move");
        InputDataWithAchivement(ref x, ref z, "Down", GetKey("Down"), "Move");
        InputDataWithAchivement(ref x, ref z, "Up", GetKey("Up"), "Move");

        //Debug.Log(Marshal.PtrToStringAnsi(GetDialogue()).ToString());


        //if (UIManager.isOpened == false)
        //{
        //    if (currentTime == 0)
        //    {
        //        currentTime = Time.time + 3f;
        //    }
        //}


        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if (Input.GetKey((KeyCode)GetKey("Jump")) && isGrounded)
        {
            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

        // If the user presses the menu key and the menu isn't open 
        // or the user presses the menu key while the menu is open and no fields are selected
        if (Input.GetKeyDown((KeyCode)GetKey("Menu")) && !isCanvasActive)
        {
            ToggleMenu();
        }

        if (Input.GetKeyDown(KeyCode.I) && isCanvasActive == false)
        {
            ToggleInventory();
           
        }

        
        //for achievement display
        if (Time.time >= currentTime && myAchievements.isUnlocked)
        {
            Debug.Log("Achivement display closed");
            myAchievements.ShowAchievement(false);
            myAchievements.isUnlocked = false;
            currentTime = 0f;
        }
        
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Friend" || other.gameObject.tag == "Cube")
        {
            print("Sorry that I hit you");
            Physics.IgnoreCollision(this.GetComponent<Collider>(), other.collider);
        }
    }

    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "Ground")
            isGrounded = true;



        if (other.gameObject.tag == "Item") {

            if (other.gameObject.name == "Log" && other != null)
            {
                SendEvent(AddScore(1), (int)Type.ScoreUpdate);
                SendEvent(CreateDialogue("Assets/Resources/test.txt"), (int)Type.DialogueExecute);
                if (myAchievements.GetAchivement("Log5") != System.IntPtr.Zero)
                {
                    currentTime = Time.time + 4.0f;
                    SendEvent(myAchievements.GetAchivement("Log5"), (int)Type.AchievementUpdate);

                }
                if (myAchievements.GetAchivement("Log10") != System.IntPtr.Zero)
                {
                    currentTime = Time.time + 4.0f;

                    SendEvent(myAchievements.GetAchivement("Log10"), (int)Type.AchievementUpdate);

                }
                if (myAchievements.GetAchivement("Log20") != System.IntPtr.Zero)
                {
                    currentTime = Time.time + 4.0f;

                    SendEvent(myAchievements.GetAchivement("Log20"), (int)Type.AchievementUpdate);

                }

                //if (myAchievements.isUnlocked)
                //{
                //    Debug.Log("Unlock Achivement time:" + Time.time + 6f);
                //    currentTime = (Time.time + 6f);
                //}
            }

            GameObject.FindGameObjectWithTag("InventoryManager").GetComponent<InventoryManager>().AddToInventory(other.gameObject);
            Destroy(other.gameObject);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Tree")
        {
            interactText.GetComponent<Text>().text = "Press '" + ((KeyCode)GetKey("Interact")).ToString() + "' to interact";

            if (Input.GetKeyDown((KeyCode)GetKey("Interact")))
            {
                GameObject log = Instantiate(logs);
                log.name = "Log";
                log.transform.position = other.transform.position + new Vector3(0, 1, 0);
                Destroy(other.gameObject);
                interactText.GetComponent<Text>().text = "";
            }
        }
        if (other.gameObject.tag == "Buildable")
        {
            BuildableController buildable = other.gameObject.GetComponent<BuildableController>();

            if (buildable != null) {
                interactText.GetComponent<Text>().text = "Press '" + ((KeyCode)GetKey("Interact")).ToString() + "' to add items\n" + buildable.GetProgressText();

                if (Input.GetKeyDown((KeyCode)GetKey("Interact")))
                {
                    buildable.Interact();
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Tree")
        {
            interactText.GetComponent<Text>().text = "";
        }
    }
}
