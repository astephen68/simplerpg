﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

[System.Serializable]
public class Dialogue {

    const string DLL_NAME = "UnityPlugins";

    [DllImport(DLL_NAME)]
    private static extern void SendEvent(System.IntPtr data, int type);
    [DllImport(DLL_NAME)]
    private static extern System.IntPtr GetDialogue();
    [DllImport(DLL_NAME)]
    private static extern System.IntPtr CreateDialogue(string filename);

    public string name = "The Guide";
    
    //[TextArea(3,10)]
    private string sentences;
    public string[] getSentences()
    {
        SendEvent(CreateDialogue("Assets/Resources/test.txt"), (int)Type.DialogueExecute);
        sentences = Marshal.PtrToStringAnsi(GetDialogue()).ToString();
        string[] lines = sentences.Split('.');
        System.Array.Resize(ref lines, lines.Length - 1);
        return lines;
    }
    

}
