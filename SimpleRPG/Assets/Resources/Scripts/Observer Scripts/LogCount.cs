﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogCount : MonoBehaviour {

    static int Count = 1;

	// Use this for initialization
	void Start () {        
        //Debug.Log(Count);
	}

   public  int GetCount()
    {
        return Count;
    }

    private void OnDestroy()
    {
        Count++;
    }   
}
