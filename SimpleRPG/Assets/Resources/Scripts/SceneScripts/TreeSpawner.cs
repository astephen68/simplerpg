﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Profiling;

/// <summary>
/// Spawner for the tree
/// </summary>
public class TreeSpawner : MonoBehaviour {

    [SerializeField]
    public TreeType type;

    //amount of trees to spawn
    public int amountToSpawn=10000;

    private void Awake()
    {
        Screen.SetResolution(1100, 513, false);
    }

    private void Start()
    {
        //To show data on CPU
        Profiler.BeginSample("Flyweight test");

        for (int i = 0; i < amountToSpawn; i++)
        {
            //Unit test for without flyweight
            //TestWithoutFlyWeight(i);

            //Test for flyweight
            TreeClient oakTree = new TreeClient(TreeType.OAK, "Prefabs/Tree", new string[] { "Materials/TreeLeaves", "Materials/TreeTrunk" }, 2, GetRandomTransform(),
                CreateQuaternionForRotation(), new Vector3(100f, 100f, 100f));
            if (i != 0)
            {
                GameObject temp=Instantiate(oakTree.Draw());
                temp.transform.SetParent(transform);
            }

        }
        Profiler.EndSample();
    }


    private void TestWithoutFlyWeight(int i)
    { 
        GameObject test = new GameObject("Oak" + i);
        test.transform.position = GetRandomTransform();
        test.transform.rotation = CreateQuaternionForRotation();
        test.transform.localScale = new Vector3(100f, 100f, 100f);
        test.AddComponent<MeshRenderer>().material.mainTexture = Resources.Load("Textures/tree") as Texture2D;

        test.AddComponent<MeshFilter>().sharedMesh = Resources.Load<GameObject>("Prefabs/Tree").GetComponent<MeshFilter>().sharedMesh;
    }


    private static Vector3 GetRandomTransform()
    {
        return new Vector3(Random.Range(-100f, 100f), -0.5f, Random.Range(-100f, 100f));
    }
    
    private static  Quaternion CreateQuaternionForRotation()
    {
        Quaternion quat = new Quaternion();
        quat.eulerAngles = new Vector3(0f, 0f, 0f);
        return quat;
    }
    
}


