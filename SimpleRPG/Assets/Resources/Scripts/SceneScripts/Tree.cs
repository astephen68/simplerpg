﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//enum to spawn differnet types of trees
public enum TreeType
{
    OAK=0
}

//abstract class for the flyweight
public abstract class Tree {

    //Intrisct values shared by all trees
    public  abstract Mesh myMesh { get; set; }
    public abstract Material[] materials { get; set; }
    public abstract GameObject Draw();

    public abstract MeshCollider meshCollider { get; set; }
    public abstract CapsuleCollider capsuleCollider { get; set; }
    public abstract Rigidbody ridgidbody { get; set; }
}
