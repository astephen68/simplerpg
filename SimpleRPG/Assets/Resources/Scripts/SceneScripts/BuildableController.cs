﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BuildableController : MonoBehaviour {

    public abstract void Interact();
    public abstract string GetProgressText();
}
