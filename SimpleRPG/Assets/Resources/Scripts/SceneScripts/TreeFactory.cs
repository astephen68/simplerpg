﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

/// <summary>
/// Flyweight Factory
/// </summary>
public class TreeFactory{

    private static Dictionary<TreeType,Tree> myTrees=new Dictionary<TreeType, Tree>();
   

    public static Tree CreateTree(TreeType type, string meshLocation, string[] textureLocation, int materialCount)
    {
       
        if (!myTrees.ContainsKey(type))
        {
            if (type == TreeType.OAK)
            {

               Tree myOakTree = new OakTree(new GameObject("OakTree"), materialCount);
                myOakTree.myMesh= Resources.Load<GameObject>(meshLocation).GetComponent<MeshFilter>().sharedMesh;

                for (int i = 0; i < materialCount; i++)
                {
                    myOakTree.materials[i] = Resources.Load(textureLocation[i]) as Material;
                }

                //load the mesh here
                myTrees.Add(type, myOakTree);
               // Debug.Log("Creating Tree");

                return myOakTree;
                
            }

        }
        else
        {
           // Debug.Log("Referencing Tree");

            return myTrees[type];
        }

        return null;
    }
}
