﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CabinController : BuildableController
{

    private bool __isBuilt;
    public bool IsBuilt
    {
        get { return __isBuilt; }
        set
        {
            __isBuilt = value;
            myRenderer.material = __isBuilt ? BuiltMaterial : GhostMaterial;
            ColliderSet.SetActive(__isBuilt);
        }
    }

    public Material BuiltMaterial;
    public Material GhostMaterial;
    public GameObject ColliderSet;

    public ParticleSystem PlayerParticleSystem;

    public int LogRequirement = 40;

    private MeshRenderer myRenderer;

    private int myLogCount = 0;
    private int myRockCount = 0;

    private InventoryManager myInventoryManager;

    // Use this for initialization
    void Start () {
        myRenderer = GetComponent<MeshRenderer>();
        myInventoryManager = GameObject.FindGameObjectWithTag("InventoryManager").GetComponent<InventoryManager>();

        IsBuilt = false;
	}

    public override void Interact()
    {
        if (!IsBuilt)
        {
            if (myLogCount < LogRequirement && myInventoryManager.GetItemCount("Log") >= 1) {
                myLogCount++;
                myInventoryManager.RemoveItem("Log", 1);

                if (PlayerParticleSystem != null)
                    PlayerParticleSystem.Emit(5);
            }

            if (myLogCount >= LogRequirement)
                IsBuilt = true;

        }
    }

    public override string GetProgressText() {
        return string.Format("{0}/{1} logs", myLogCount, LogRequirement);
    }
}
