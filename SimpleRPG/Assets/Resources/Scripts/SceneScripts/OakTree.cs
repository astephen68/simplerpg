﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class OakTree : Tree
{
    public override Mesh myMesh { get; set; }

    public override Material[] materials { get; set; }

    public override MeshCollider meshCollider { get; set; }
    public override CapsuleCollider capsuleCollider { get; set; }
    public override Rigidbody ridgidbody { get; set; }

//Extrinstc value 
private GameObject myGameObject;

    public OakTree(GameObject gameobject, int materialCount)
    {
        myGameObject = gameobject;
        myGameObject.AddComponent<MeshFilter>();
        myGameObject.AddComponent<MeshRenderer>();
        myGameObject.AddComponent<MeshCollider>();
        myGameObject.AddComponent<CapsuleCollider>();
        myGameObject.AddComponent<Rigidbody>();
        materials = new Material[materialCount];
    }


    //Intristic value that will allow you to attach the mesh and texture to the object
    public  override GameObject Draw()
    {
        myGameObject.tag = "Tree";
        myGameObject.GetComponent<MeshFilter>().sharedMesh = myMesh;
        myGameObject.GetComponent<MeshRenderer>().materials = materials;
        myGameObject.GetComponent<MeshCollider>().sharedMesh = myMesh;
        myGameObject.GetComponent<CapsuleCollider>().isTrigger = true;
        myGameObject.GetComponent<CapsuleCollider>().radius = 0.03f;
        myGameObject.GetComponent<CapsuleCollider>().height = 0.08f;
        myGameObject.GetComponent<Rigidbody>().isKinematic = true;
        myGameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotationX
            | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationZ;
        return myGameObject;
    }
}

//The CLient class that gathers all the data  together
class TreeClient
{
    //pointer to the one instance of tree that we need to refernece
    private Tree myTree;
    private Vector3 position;
    private Quaternion rotation;
    private Vector3 scale;


    public TreeClient(TreeType type, string meshLocation, string[] textureLocation, int materialCount, 
        Vector3 pos, Quaternion rot, Vector3 scale)
    {
        myTree = TreeFactory.CreateTree(type, meshLocation, textureLocation, materialCount);
        position = pos;
        rotation = rot;
        this.scale = scale;
    }

    
    public GameObject Draw()
    {
        GameObject myGame = myTree.Draw();
        myGame.transform.position = position;
        myGame.transform.rotation = rotation;
        myGame.transform.localScale = scale;
        
        return myGame;

    }
}