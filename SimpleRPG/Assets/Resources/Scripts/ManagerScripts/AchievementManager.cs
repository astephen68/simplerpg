﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Runtime.InteropServices;


public class AchievementManager : MonoBehaviour
{
    #region DLL Stuff
    const string DLL_NAME = "UnityPlugins";

    [DllImport(DLL_NAME)]
    private static extern System.IntPtr CreateAchievement(string description, int type);
    [DllImport(DLL_NAME)]

    private static extern System.IntPtr CreateAchievementAmount(string description, int type, int amount);
    [DllImport(DLL_NAME)]

    private static extern System.IntPtr GetAchivementInfo(System.IntPtr achivement);
    [DllImport(DLL_NAME)]

    private static extern void SendEvent(System.IntPtr data, int type);

    [DllImport(DLL_NAME)]

    private static extern bool isAchivementUnlocked(System.IntPtr data);
    #endregion
    public GameObject text;
    public GameObject panel;

    string achievementText;
    List<string> keysToRemove;
    public bool isUnlocked = false;

    Dictionary<string, System.IntPtr> currentAchievements;

    void Start()
    {
        currentAchievements = new Dictionary<string, System.IntPtr>();
        keysToRemove = new List<string>();
    }


    public void addAchievement(System.IntPtr achievement, string name)
    {
        currentAchievements.Add(name, achievement);
    }

    public System.IntPtr GetAchivement(string name)
    {
        if (currentAchievements.ContainsKey(name))
        {
            return currentAchievements[name];

        }
        return System.IntPtr.Zero;
    }

    private void Update()
    {
        
        foreach (KeyValuePair<string, System.IntPtr> pair in currentAchievements)
        {
            
            if (isAchivementUnlocked(pair.Value))
            {
                ActivateAchievement(pair.Key);
                isUnlocked = true;
                keysToRemove.Add(pair.Key);
                
            }
        }

        for (int i = 0; i < keysToRemove.Count; i++)
        {
            currentAchievements.Remove(keysToRemove[i]);   
        }
        keysToRemove.Clear();
    }
    

    void ActivateAchievement(string name)
    {
        System.IntPtr myAchievement = currentAchievements[name];

        achievementText = Marshal.PtrToStringAnsi(GetAchivementInfo(myAchievement)).ToString();
        ShowAchievement(true);
        
    }

    public void ShowAchievement(bool achievement)
    {
        text.GetComponent<Text>().text = achievementText;
        panel.SetActive(achievement);
    }
}
