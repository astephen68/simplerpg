﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
public class ScoreManager : MonoBehaviour {
    #region API
    const string DLL_NAME = "UnityPlugins";
    [DllImport(DLL_NAME)]
    private static extern int GetScore();

    [DllImport(DLL_NAME)]
    private static extern System.IntPtr AddScore(int score);
    #endregion
    public GameObject scoreUI;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateScore();
	}

    void UpdateScore()
    {
        // Debug.Log(GetScore());
        scoreUI.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "Logs Collected: " + GetScore();
    }
}
