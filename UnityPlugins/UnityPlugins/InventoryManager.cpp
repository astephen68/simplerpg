#include "InventoryManager.h"

InventoryManager::InventoryManager(EventManager* manager)
{
	myManager = manager;
	myManager->attach(this);
}

InventoryManager::~InventoryManager()
{

}

void InventoryManager::Update()
{
	if (myManager->getState().myType == InventoryUpdated)
	{
		Item* current = static_cast<Item*>(myManager->getState().data);
		AddItem(current);
	}
	else
	{
		return;
	}
}

void InventoryManager::AddItem(Item* item) {
	
	it = items.find(item->GetItemName());

	// if the map contains the items add to the amount 
	if (it != items.end())
	{
		it->second.setItemAmount(item->GetItemAmount()+it->second.GetItemAmount());

	}
	else
	{
		items.insert(std::pair<std::string, Item>((std::string)(*item).GetItemName(), *item));
	}
}

Item::Item(std::string name, int amount) {
	this->name = name;
	amountCollected = amount;
}

Item::~Item() {

}

char* Item::GetItemName() const {
	return ((char*)name.c_str());
}

void Item::setItemAmount(int amount) {
	amountCollected = amount;
}

int InventoryManager::GetItemAmount(char* itemName) {
	it = items.find(itemName);

	// if the map contains the items add to the amount 
	if (it != items.end())
	{
		return it->second.GetItemAmount();
	}
	return 0;
}