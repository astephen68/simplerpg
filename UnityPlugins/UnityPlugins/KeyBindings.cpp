#include "KeyBindings.h"

KeyBindings::KeyBindings()
{
	changedKeyBindings = new std::unordered_map<std::string, int>();
	changedKeyBindings->insert(std::pair<std::string, int>("Left", 97));
	changedKeyBindings->insert(std::pair<std::string, int>("Up", 119));
	changedKeyBindings->insert(std::pair<std::string, int>("Right", 100));
	changedKeyBindings->insert(std::pair<std::string, int>("Down", 115));
	changedKeyBindings->insert(std::pair<std::string, int>("Interact", 101));
	changedKeyBindings->insert(std::pair<std::string, int>("Jump", 32));
	changedKeyBindings->insert(std::pair<std::string, int>("Menu", 27));


	defaultKeyBindings = new std::unordered_map<std::string, int>();
	defaultKeyBindings->insert(std::pair<std::string, int>("Left", 97));
	defaultKeyBindings->insert(std::pair<std::string, int>("Up", 119));
	defaultKeyBindings->insert(std::pair<std::string, int>("Right", 100));
	defaultKeyBindings->insert(std::pair<std::string, int>("Down", 115));
	defaultKeyBindings->insert(std::pair<std::string, int>("Interact", 101));
	defaultKeyBindings->insert(std::pair<std::string, int>("Jump", 32));
	defaultKeyBindings->insert(std::pair<std::string, int>("Menu", 27));
	keyToUndo = std::vector < std::string>();
	keysToChange = std::vector<int>();

	previous = -1;
}
    

KeyBindings::~KeyBindings()
{
	delete defaultKeyBindings;
	delete changedKeyBindings; 
}

void KeyBindings::ChangeKeyBinding(std::string key, int keyToCHange)
{
	 if (defaultKeyBindings->find(key)->second!=keyToCHange&& !isUndoing)
	{
		 for (int i = 0; i < keysToChange.size(); i++)
		 {
			 if (keyToUndo[i] == key)
			 {
				 if (previous != -1) {
					 //potential memory leak'
					 previous = -1;
				 }
				 previous = (*changedKeyBindings).find(key)->second;
				 keysToChange[i] = previous;
				 changedKeyBindings->find(key)->second = keyToCHange;
				 return;
			 }
		 }
		
		previous = -1;

		previous=changedKeyBindings->find(key)->second;
		keysToChange.push_back(previous);
		keyToUndo.push_back(key);
		changedKeyBindings->find(key)->second = keyToCHange;

	}

	else if(defaultKeyBindings->find(key)->second == keyToCHange && isUndoing)
	{
		changedKeyBindings->find(key)->second = keyToCHange;

	}
	else if (defaultKeyBindings->find(key)->second != keyToCHange && isUndoing)
	{
		changedKeyBindings->find(key)->second = keyToCHange;
	}

}


void KeyBindings::ResetKey(std::string key)
{
	changedKeyBindings->find(key)->second = defaultKeyBindings->find(key)->second;
}


void KeyBindings::ResetAllKeys()
{
	delete changedKeyBindings;
	changedKeyBindings = new std::unordered_map<std::string, int>();
	changedKeyBindings->insert(std::pair<std::string, int>("Left", 97));
	changedKeyBindings->insert(std::pair<std::string, int>("Up", 119));
	changedKeyBindings->insert(std::pair<std::string, int>("Right", 100));
	changedKeyBindings->insert(std::pair<std::string, int>("Down", 115));
	changedKeyBindings->insert(std::pair<std::string, int>("Interact", 101));
	changedKeyBindings->insert(std::pair<std::string, int>("Jump", 32));
	changedKeyBindings->insert(std::pair<std::string, int>("Menu", 27));
}

int KeyBindings::GetKey(std::string key)
{
	if (changedKeyBindings->find(key) != changedKeyBindings->end())
	{
		return changedKeyBindings->find(key)->second;
	}
	return -1;
}

char * KeyBindings::GetKeyName(int pos)
{
	int count = 0;
	for (auto it = changedKeyBindings->begin(); it != changedKeyBindings->end(); ++it)
	{
		if (count == pos)
		{
			return (char*)it->first.c_str();
		}
		count++;
	}

	return nullptr;
}


int KeyBindings::getTotalKeys()
{
	return (int)defaultKeyBindings->size();
}


int KeyBindings::GetDefaultKeyValue(std::string keyName)
{
	return defaultKeyBindings->find(keyName)->second;
}

void KeyBindings::Undo(std::string keyname, int keycode)
{
	isUndoing = true;
	ChangeKeyBinding(keyname, keycode);
	isUndoing = false;
}


void KeyBindings::Undo()
{
	isUndoing = true;
	for (int i = 0; i < keysToChange.size(); i++)
	{
		ChangeKeyBinding(keyToUndo[i], keysToChange[i]);

	}
	isUndoing = false;
	keysToChange.clear();
	keyToUndo.clear();
}
