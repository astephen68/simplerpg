#pragma once
#include "Observer.h"
#include "EventManager.h"

enum AchivementType
{
	Collection,
	Movement,

};
class Achievement
{

public:
	Achievement(std::string info, AchivementType type);
	Achievement(std::string info, AchivementType type, int amount);
	~Achievement();
	AchivementType GetType();
	char* GetAchivementInfo();
	void Unlock();
	bool isAchivementUnlocked();
	void IncreaseCollected();

private:
	std::string info;
	int amountCollected=0;
	int totalCollected=0;
	AchivementType myType;
	bool isUnlocked;
	void Update();

};

class AchievementManager : public Observer
{
	private:
		std::vector<Achievement> achievements;
		std::string currentAchivement;

	public:
		AchievementManager(EventManager* manager);
		~AchievementManager();
		void Update() override;
		const char* GetAchivementUnlockedInfo();

};