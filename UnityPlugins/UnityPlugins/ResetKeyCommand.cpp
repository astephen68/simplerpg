#include "ResetKeyCommand.h"

ResetKeyCommand::ResetKeyCommand(KeyBindings * myKey, std::string key)
{
	keybindings = myKey;
	this->key = key;
	previousKey = -1;
}

ResetKeyCommand::~ResetKeyCommand() {
}

void ResetKeyCommand::Execute()
{
	previousKey = (*keybindings).GetKey(key);
	keybindings->ResetKey(key);
}

void ResetKeyCommand::Undo()
{
	keybindings->Undo(key,previousKey);
}

void ResetKeyCommand::Redo()
{
	Execute();
}
