#pragma once
#include "Command.h"
#include "KeyBindings.h"
class ChangeKeyCommand:public Command
{
public:
	ChangeKeyCommand(KeyBindings*, std::string, int keyCpde);
	~ChangeKeyCommand();
	void Execute() override;
	void Undo() override;
	void Redo() override;
private:
	KeyBindings * myBindings;
	std::string keyname;
	int myKeycode;
	int previousKey;

};