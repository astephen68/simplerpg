#pragma once
#include <vector>
#include "Command.h"
#include "ChangeKeyCommand.h"
#include "ResetKeyCommand.h"
class KeyChanges
{
private:
	std::vector<Command*> commands;
	std::vector<Command*> redoCommands;
	KeyBindings *myBindings;

public:
	KeyChanges();
	~KeyChanges();
	void Undo();
	void Redo();
	void ChangeKey(std::string name, int keycode);
	void ResetKey(std::string name);
	int GetKey(std::string name);
	void ResetAllKeys();
	char* GetKeyName(int pos);
	int GetTotalKeys();
	int GetDefaultKeyValue(std::string name);

};