#include "AchievementManager.h"

AchievementManager::AchievementManager(EventManager * manager)
{
	myManager = manager;
	myManager->attach(this);
}

AchievementManager::~AchievementManager()
{
}

void AchievementManager::Update()
{
	if (myManager->getState().myType==AchievementUpdate)
	{
		Achievement * current = static_cast<Achievement*>(myManager->getState().data);
		switch (current->GetType())
		{
		case Collection:
			current->IncreaseCollected();
			break;
		case Movement:
			current->Unlock();
			currentAchivement = current->GetAchivementInfo();
			break;
		default:
			break;
		}
	}
	else
	{
		return;
	}
}

const char * AchievementManager::GetAchivementUnlockedInfo()
{
	return currentAchivement.c_str();
}

Achievement::Achievement(std::string info, AchivementType type)
{
	this->info = info;
	this->myType = type;
	isUnlocked = false;

}

Achievement::Achievement(std::string info, AchivementType type, int amount)
{
	this->info = info;
	myType = type;
	amountCollected = amount;
}

Achievement::~Achievement()
{
}

AchivementType Achievement::GetType()
{
	return myType;
}

char* Achievement::GetAchivementInfo()
{
	return ((char*)info.c_str());
}

void Achievement::Unlock()
{
	isUnlocked = true;
}

bool Achievement::isAchivementUnlocked()
{
	return isUnlocked;
}

void Achievement::IncreaseCollected()
{
	totalCollected++;
	Update();

}

void Achievement::Update()
{
	if (amountCollected==totalCollected)
	{
		Unlock();
	}
}
