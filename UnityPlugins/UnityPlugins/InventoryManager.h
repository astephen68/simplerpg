#pragma once
#include <map>
#include "Observer.h"
#include "EventManager.h"

class Item
{
public:
	Item(std::string name, int amount);
	~Item();
	void setItemAmount(int amount);
	char* GetItemName() const;
	int GetItemAmount() const { return amountCollected; }
private:
	std::string name;
	int amountCollected = 0;
};

class InventoryManager : public Observer
{
public:
	InventoryManager(EventManager* manager);
	~InventoryManager();
	void Update() override;
	void AddItem(Item* item);
	int GetItemAmount(char* itemName);
	
private:
	std::map<std::string, Item> items;
	std::map<std::string, Item>::iterator it;
};

