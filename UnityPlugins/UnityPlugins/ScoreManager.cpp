#include "ScoreManager.h"

ScoreManager::ScoreManager(EventManager * manager)
{
	myManager = manager;
	myManager->attach(this);
}

ScoreManager::~ScoreManager()
{
}

void ScoreManager::AddScore(int amount)
{
	score += amount;
}

void ScoreManager::Update()
{
	if (myManager->getState().myType==ScoreUpdate)
	{
		ScoreEvent* thisScore = static_cast<ScoreEvent*>(myManager->getState().data);
		AddScore(thisScore->score);
	}
	return;
}

int ScoreManager::GetScore()
{
	return score;
}
