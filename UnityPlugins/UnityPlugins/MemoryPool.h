#pragma once
/*
	This class represents a paged pool of memory that will automatically expand itself when required.
	Each page uses a bitfield to determine which blocks are allocated
	
*/
#include <cmath>
// If we have not defined a default page size elsewhere, default to 1024
#ifndef DEFAULT_PAGE_SIZE
#define DEFAULT_PAGE_SIZE 1024
#endif
// If we have not defined a allocated field type elsewhere, use bytes by default
#ifndef ALLOC_FIELD_TYPE
#define ALLOC_FIELD_TYPE uint8_t
#endif

/*
	/brief Raises num to the given power in a constexpr compatable way

	Taken from http://prosepoetrycode.potterpcs.net/2015/07/a-simple-constexpr-power-function-c/

	/param num The number to raise
	/param pow The power to raise num to
	/return num ^ pow
*/
template <typename T>
constexpr T ipow(T num, unsigned int pow) {
	return (pow >= sizeof(unsigned int) * 8) ? 0 :
		pow == 0 ? 1 : num * ipow(num, pow - 1);
}

/*
	Typedef for what unit our allocated bit field can store. This may be overloaded
	by defining ALLOC_FIELD_TYPE before including this header
*/
typedef ALLOC_FIELD_TYPE AllocBitType;
/*
	A constant expression to get the number of bits in the alloc bit field type
*/
constexpr size_t AllocBitSize = sizeof(AllocBitType) * 8;
/*
	Returns the leftmost bit in the alloc field type (ex: for uint8_t, will return 128)
*/
constexpr size_t AllocMax = ipow(2, AllocBitSize - 1);

/*
	Returns the number of elements required of type AllocBitType to store the given
	number of bit entries.
*/
constexpr size_t BitCount(size_t numEntries) {
	size_t result = numEntries / AllocBitSize;
	result += numEntries % AllocBitSize ? 1 : 0;
	return result;
}
/*
	Returns whether a given bit is set at the offset from the lefthand side of the
	bitfield
*/
constexpr bool IsBitSet(AllocBitType value, AllocBitType offsetX) {
	return value & (AllocMax >> offsetX);
}
/*
	Sets the given value at offset from the lefthand side of the bitfield to isSet
*/
constexpr void SetBit(AllocBitType& value, AllocBitType offsetX, AllocBitType isSet) {
	value ^= (-!!isSet ^ value) & (AllocMax >> offsetX);
}

/*
	A templated abstract class to make working with different sized memory
	pool easier
*/
template <typename ValueType>
class IMemoryPool {
public:
	virtual ~IMemoryPool() {}

	/*
		\brief Allocates and returns a block of memory from the pool

		This will return a block of memory from this pool. Note that this block may
		contain garbage data, and still needs to be initialized

		\return A new instance from the pool
	*/
	virtual ValueType* Allocate() = 0;
	/*
		/brief Frees an allocated value from this pool

		Will attempt to free the given value from the pool and call it's
		destructor. If the value is not within the pool, this will do nothing

		/param value The value to free
	*/
	virtual void Free(ValueType* value) = 0;
	
	/*
		\brief Allocates and initializes a new instance from the pool
		
		This will allocate a new block of memory, and call the paramaterless constructor
		on it

		\return A new initialized instance from the pool
	*/
	ValueType* AllocInit() {
		// Allocate memory, call constructor, then return
		ValueType* result = Allocate();
		new (result) ValueType();
		return result;
	}
	/*
		\brief Allocates and initializes a new instance from the pool, using variadic arguments

		This will allocate a new block of memory, and forward the arguments to the instance's 
		constructor. Note that there will not be any syntax suggestions for parameter names

		\param args The variadic arguments to forward to the constructor
		\return A new initialized instance from the pool
	*/
	template <typename ... TArgs>
	ValueType* AllocInit(TArgs&& ... args) {
		// Allocate, call constructor with forwarded args, and return
		ValueType* result = Allocate();
		new (result) ValueType(std::forward<TArgs>(args)...);
		return result;
	}
};

/*
	Implements a paged memory pool of ValueType, where each page holds PAGE_SIZE elements
*/
template <typename ValueType, size_t PAGE_SIZE = DEFAULT_PAGE_SIZE>
class MemoryPool : public IMemoryPool<ValueType> {
public:
	/*
		\brief Allocates and returns a block of memory from the pool

		This will return a block of memory from this pool. Note that this block may
		contain garbage data, and still needs to be initialized

		\return A new instance from the pool
	*/
	ValueType* Allocate() {
		return myPage.Alloc();
	}
	/*
		/brief Frees an allocated value from this pool

		Will attempt to free the given value from the pool and call it's
		destructor. If the value is not within the pool, this will do nothing

		/param value The value to free
	*/
	void Free(ValueType* value) {
		myPage.Free(value);
	}
	
private:
	/*
		The actual memory pages, this is where the magic happens
	*/
	struct MemoryPage{
		// We want to store raw bytes, so we use a char array
		char   Data[PAGE_SIZE * sizeof(ValueType)];
		// This is the bitfield that stores which blocks are allocated
		AllocBitType AllocatedBits[BitCount(PAGE_SIZE)];
		// Pointer to the next page in memory
		MemoryPage* Next;

		// Helpers to improve allocation speed
		size_t myNextFree;
		size_t myNumAlloc;

		/*
			Creates and initializes a new memory page
		*/
		MemoryPage() {
			// Zero our data to avoid garbage
			memset(Data, 0, PAGE_SIZE * sizeof(ValueType));
			memset(AllocatedBits, 0, sizeof(AllocatedBits));
			Next = nullptr;
			myNumAlloc = 0;
			myNextFree = 0;
		}
		/*
			Deletes a memory page and calls the destructors on any elements
			that are still allocated
		*/
		~MemoryPage() {
			//Iterate over the page and delete elements if they exist
			for (int ix = 0; ix < PAGE_SIZE; ix++) {
				if (IsAllocated(ix))
					Get(ix)->~ValueType();
			}
			// If there is a tailing page, delete it as well
			if (Next != nullptr)
				delete Next;
		}

		/*
			Handles allocating a new block of memory. If this page is full, this
			will cascade to the next page
		*/
		ValueType* Alloc() {
			// We first have an easy out for fully allocated blocks
			if (myNumAlloc < PAGE_SIZE) {
				// Get the next open block
				ValueType* result = ((ValueType*)Data) + myNextFree;
				// Mark it as used
				SetAlloc(myNextFree, true);
				// Increment the number of allocs and our next free ID
				myNumAlloc++;
				myNextFree++;
				// If we are full, then allocate the next block ahead of time
				if (myNumAlloc == PAGE_SIZE)
					Next = new MemoryPage();
				// Check the rest of the block for a free slot
				else {
					while (IsAllocated(myNextFree))
						myNextFree++;
				}
				// Return the item
				return result;
			}
			// We jump to the next block and get one from it's
			else {
				return Next->Alloc();
			}
		}
		/*
			Frees the element if it is on this page, otherwise cascades to the
			next page
		*/
		void Free(ValueType* data) {
			// Calculate the offset index from pointer diffs
			size_t offset = (size_t)(data - (ValueType*)Data) / sizeof(ValueType);
			// Because we're unsigned, do not need to check for negative
			if (offset >= PAGE_SIZE) {
				// Attempt to cascade to next page
				if (Next != nullptr)
					Next->Free(data);
			}
			else {
				// We should only really free if it's actually allocated
				if (IsAllocated(offset)) {
					// This is more or less why (don't want double destructor calls)
					((ValueType*)Data)[offset].~ValueType();
					// Mark as free
					SetAlloc(offset, false);
					// If the index is less than what is free, update the next free index
					myNextFree = myNextFree < offset ? myNextFree : offset;
					// We have more space now!
					myNumAlloc--;
				}
			}
		}

		/*
			Gets the value at the offset if it is allocated, and nullptr if it is not
			If the offset is outside this page's range, will attempt to cascade to the 
			next page
		*/
		ValueType* Get(size_t offset) {
			if (offset < PAGE_SIZE)
				return IsAllocated(offset) ? ((ValueType*)Data) + offset : nullptr;
			else if (Next != nullptr)
				return Next->Get(offset - PAGE_SIZE);
			else
				return nullptr;
		}
		/*
			Checks whether a given offset is allocated within this page. If the offset is 
			outside this page's range, will attempt to cascade to the next page
		*/
		bool IsAllocated(size_t offset) {
			if (offset < PAGE_SIZE)
				return IsBitSet(AllocatedBits[offset / AllocBitSize], offset % AllocBitSize);
			else if (Next != nullptr)
				return Next->IsAllocated(offset - PAGE_SIZE);
			else
				return false;
		}
		/*
			Marks a given offset as either allocated or free. If the offset is
			outside this page's range, will attempt to cascade to the next page
		*/
		void SetAlloc(size_t offset, bool isAlloc) {
			if (offset < PAGE_SIZE)
				SetBit(AllocatedBits[offset / AllocBitSize], offset % AllocBitSize, isAlloc);
			else if (Next != nullptr)
				Next->SetAlloc(offset - PAGE_SIZE, isAlloc);
		}
	};
	
	/*
		The base page exists as a vlue type, so it will automagically be
		initialized and destroyed with this pool
	*/
	MemoryPage myPage;
};