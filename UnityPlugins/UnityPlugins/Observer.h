#pragma once
class EventManager;

class Observer
{
protected:
	EventManager* myManager;
public:
	virtual void Update() = 0;
};