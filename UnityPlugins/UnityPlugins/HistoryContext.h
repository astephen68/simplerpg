#pragma once

#include "Commands.h"
#include "TileMap.h"

/*
	Definition of the history system
	Developed by Shawn
*/

#define MAX_HISTORY 512

typedef ICommand* CommandPtr;

class HistoryContext {
public:
	HistoryContext();
	~HistoryContext();

	void Push(ICommand* command, TileMap* map);
	bool Pop(TileMap* map);

	bool Undo(TileMap* map);
	bool Redo(TileMap* map);

private:
	uint16_t myMinId, myMaxId, myCurrentId, myNextId;
	CommandPtr myCommandPool[MAX_HISTORY];

	ICommand*& __GetCommand(int offset);
};