#include "EventManager.h"

EventManager::EventManager()
{
	observers = std::vector<Observer*>();
}

EventManager::~EventManager()
{
}

Event EventManager::getState()
{
	return state;
}

void EventManager::SetState(Event state)
{
	this->state = state;
	notifyAllObservers();
}

void EventManager::attach(Observer * observer)
{
	observers.push_back(observer);
}

void EventManager::notifyAllObservers()
{
	for (int i = 0; i < observers.size(); i++)
	{
		observers[i]->Update();
	}
}
