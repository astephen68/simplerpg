#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "Observer.h"
#include "EventManager.h"


struct DialogueEvent
{
	std::string filename;

};
class DialogueManager :public Observer
{
private:
	std::string lines="";
	std::ifstream myFile;
	void ReadFile(std::string filename);
public:
	DialogueManager(EventManager* manager);
	~DialogueManager();
	char* GetDialougeLines();
	void Update() override;
};