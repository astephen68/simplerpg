#pragma once
#include <unordered_map>
#include "Command.h"
#include <vector>

class KeyBindings {

private:
	std::unordered_map<std::string, int>* defaultKeyBindings;
	std::unordered_map<std::string, int>* changedKeyBindings;
	std::vector<std::string>keyToUndo;
	std::vector<int> keysToChange;
	bool isUndoing=false;
	int previous=-1;
	int size; 
public:
	KeyBindings();
	~KeyBindings();
	void ChangeKeyBinding(std::string key, int keyToCHange);
	void ResetKey(std::string key);
	void ResetAllKeys();
	int GetKey(std::string key);
	char* GetKeyName(int pos);
	int getTotalKeys();
	int GetDefaultKeyValue(std::string name);
	void Undo();
	void Undo(std::string name, int key); 

};
