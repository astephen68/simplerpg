#include "DialogueManager.h"

DialogueManager::DialogueManager(EventManager * manager)
{
	myManager = manager;
	myManager->attach(this);
}

DialogueManager::~DialogueManager()
{
	myFile.close();
}

char * DialogueManager::GetDialougeLines()
{
	return ((char*)lines.c_str());
}

void DialogueManager::Update()
{
	if (myManager->getState().myType== DialogueExecute)
	{
		DialogueEvent* dEvent=static_cast<DialogueEvent*>(myManager->getState().data);
		ReadFile(dEvent->filename);
	}
}

void DialogueManager::ReadFile(std::string fileName)
{
	myFile= std::ifstream (fileName);
	if (myFile.is_open())
	{
		std::string line = "";
		while (std::getline(myFile,line))
		{
			lines += line;
		}
	}
	else
	{
		return;
	}

}
