#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>  

#include "Wrapper.h"
#include "MemoryPool.h"

// Example struct to text pool allocations
struct FooBar
{
	int X, Y, Z;
	FooBar() : X(0), Y(0), Z(0) {}
	FooBar(int v) : X(v), Y(v), Z(v) {}
	FooBar(int x, int y, int z) : X(x), Y(y), Z(z) {}
	~FooBar() {
		std::cout << "Blowin' up a foobar" << std::endl;
	}
};

int main() {
	long lBreakAlloc = 0;
	if (lBreakAlloc > 0)
	{
		_CrtSetBreakAlloc(lBreakAlloc);
	}

	// TODO: testing
	Init();

	// Memory fool testing (LEAK FREE!)
	if (true)
	{
		IMemoryPool<FooBar>* inst = new MemoryPool<FooBar, 4>();

		FooBar* a = inst->Allocate();
		FooBar* b = inst->AllocInit(1);
		FooBar* c = inst->Allocate();
		FooBar* d = inst->AllocInit(3);
		FooBar* e = inst->AllocInit(4, 5, 6);

		inst->Free(a);

		std::cout << "Doot" << std::endl;

		delete inst;
	}

	// Keybinding testing MEMORY LEAK FREE
	if (true)
	{
		char* name = GetKeyName(0);
		ChangeKeyBinding("Left", 98);
		int code = GetKey(name);

		ResetKey("Left");
		ResetAllKeyBindings();

		int defaultKey = GetDefaultKeyValue("Right");

		ChangeKeyBinding("Left", 98);
		ChangeKeyBinding("Right", 97);

		Undo();
		Redo();

		int numKeys = GetTotalAmountOfKey();
	}

	// Event management MEMORY LEAK FREE
	if (true)
	{
		Achievement* lol1 = (Achievement*)CreateAchievement("Run away!!!", AchivementType::Movement);
		Achievement* lol2 = (Achievement*)CreateAchievementAmount("Collecting sins", AchivementType::Collection, 666);
		
		if (isAchivementUnlocked(lol1))
			std::cout << "Oh hai" << std::endl;

		char* text = GetAchivementInfo(lol2);
		std::cout << text << std::endl;

		SendEvent(lol1, Type::AchievementUpdate);
		ScoreEvent* score = (ScoreEvent*)AddScore(2);
		int foo = GetScore();
	}
	
	// Dialogue system (LEAK FREE)
	if (true) {
		DialogueEvent* foo = CreateDialogue("test.txt");
		char* dialogue = GetDialogue();
	}

	// Inventory system (LEAK FREE)
	if (true) {
		Item* item = AddItem("Log", 1);
		std::cout << item->GetItemName() << " - " << item->GetItemAmount();
	}

	Cleanup();

	_CrtDumpMemoryLeaks();

	std::cin.get();

	return 0;
}