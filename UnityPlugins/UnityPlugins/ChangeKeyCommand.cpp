#include "ChangeKeyCommand.h"

ChangeKeyCommand::ChangeKeyCommand(KeyBindings * keys, std::string key, int keyCpde)
{
	myBindings = keys;
	keyname = key;
	myKeycode = keyCpde;
	previousKey = -1;
}


ChangeKeyCommand::~ChangeKeyCommand()
{
}


void ChangeKeyCommand::Execute()
{
	previousKey = (*myBindings).GetKey(keyname);
	myBindings->ChangeKeyBinding(keyname, myKeycode);
}

void ChangeKeyCommand::Undo()
{

	myBindings->Undo(keyname, previousKey);
}

void ChangeKeyCommand::Redo()
{
	Execute();
}
