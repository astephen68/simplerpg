#pragma once
#include "Command.h"
#include "KeyBindings.h"
class ResetKeyCommand :public Command
{
private:
	KeyBindings * keybindings;
	std::string key;
	int previousKey;
	
public:
	ResetKeyCommand(KeyBindings* myKey, std::string key);
	~ResetKeyCommand();
	void Execute() override;
	void Undo() override;
	void Redo() override;
};