#pragma once
#include "Observer.h"
#include "EventManager.h"

struct ScoreEvent
{
	int score;
};

class ScoreManager:public Observer
{
	private:
		int score=0;

	public:
		ScoreManager(EventManager* manager);
		~ScoreManager();
		void AddScore(int amount);
		void Update() override;
		int GetScore();
};