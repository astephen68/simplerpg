#include "KeyChanges.h"

KeyChanges::KeyChanges()
{
	myBindings = new KeyBindings();
}

KeyChanges::~KeyChanges()
{
	delete myBindings;
	for(int i=0;i<commands.size();i++) 	{
		delete commands[i];
	}
	for (int i = 0; i < redoCommands.size(); i++){
		delete redoCommands[i];
	}
}

void KeyChanges::Undo()
{
	if (commands.size()==0) {
		return;
	}
	commands.back()->Undo();
	redoCommands.push_back(commands.back());
	commands.pop_back();
}

void KeyChanges::Redo()
{
	if (redoCommands.size()==0) {
		return;
	}
	redoCommands.front()->Redo();
	commands.push_back(redoCommands.front());
	redoCommands.erase(redoCommands.begin());
}

void KeyChanges::ChangeKey(std::string name, int keycode)
{
	ChangeKeyCommand* myCommand = new ChangeKeyCommand(myBindings,name, keycode);
	myCommand->Execute();

	for (int ix = 0; ix < redoCommands.size(); ix++)
		delete redoCommands[ix];
	redoCommands.clear();

	commands.push_back(myCommand);
}

void KeyChanges::ResetKey(std::string name)
{
	ResetKeyCommand *reset = new ResetKeyCommand(myBindings, name);
	reset->Execute();

	for (int ix = 0; ix < redoCommands.size(); ix++)
		delete redoCommands[ix];
	redoCommands.clear();

	commands.push_back(reset);

}

int KeyChanges::GetKey(std::string name)
{
	return myBindings->GetKey(name);
}

void KeyChanges::ResetAllKeys()
{
	myBindings->ResetAllKeys();
}

char * KeyChanges::GetKeyName(int pos)
{
	return myBindings->GetKeyName(pos);
}

int KeyChanges::GetTotalKeys()
{
	return myBindings->getTotalKeys();
}

int KeyChanges::GetDefaultKeyValue(std::string name)
{
	return myBindings->GetDefaultKeyValue(name);
}
