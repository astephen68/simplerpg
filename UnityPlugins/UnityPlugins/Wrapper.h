#pragma once
#include "LibSettings.h"
#include "KeyBindings.h"
#include "KeyChanges.h"
#include "EventManager.h"
#include "AchievementManager.h"
#include "ScoreManager.h"
#include "DialogueManager.h"
#include "InventoryManager.h"
#ifdef __cplusplus
extern "C"
{
#endif

	LIB_API void Init();
	LIB_API void Cleanup();

	//Functions for the command design pattern
	LIB_API void ChangeKeyBinding(const char* keyToChange, int value);
	LIB_API void ResetKey(const char*);
	LIB_API int GetKey(const char*);
	LIB_API void ResetAllKeyBindings();
	LIB_API int GetDefaultKeyValue(const char* value);
	LIB_API void Undo();
	LIB_API void Redo();
	LIB_API char* GetKeyName(int pos);
	LIB_API int GetTotalAmountOfKey();

	//functions for event management
	LIB_API Achievement* CreateAchievement(const char* description, int type);
	LIB_API Achievement* CreateAchievementAmount(const char* description, int type, int amount);
	LIB_API bool isAchivementUnlocked(Achievement* achv);
	LIB_API char* GetAchivementInfo(Achievement* achivement);
	LIB_API void DeleteAcheivement(Achievement* achievement);

	LIB_API ScoreEvent* AddScore(int score);
	LIB_API int GetScore();
	LIB_API void DeleteScore(ScoreEvent* score);

	LIB_API void SendEvent(void* data, int type);

	// Inventory
	LIB_API Item* AddItem(const char* name, int amount);
	LIB_API char* GetItemName(const Item* name);
	LIB_API void SetItemAmount(Item* item, int amount);
	LIB_API int GetItemAmount(const Item* amount);
	LIB_API void DeleteItem(Item* item);

	// Dialogue
	LIB_API char* GetDialogue();
	LIB_API DialogueEvent* CreateDialogue(const char* filename);
	LIB_API void DeleteDialogue(DialogueEvent* item);


#ifdef __cplusplus 
}
#endif