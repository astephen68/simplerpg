#pragma once
#include <vector>
#include "Observer.h"

 enum Type
{
	AchievementUpdate,
	InventoryUpdated,
	DialogueExecute,
	ScoreUpdate
};

struct Event
{
	Type myType;
	void* data;

};

class EventManager
{
private:
	std::vector<Observer*> observers;
	 Event state;
public:
	EventManager();
	~EventManager();
	Event getState();
	void SetState(Event state);
	void attach(Observer* observer);
	void notifyAllObservers();
};