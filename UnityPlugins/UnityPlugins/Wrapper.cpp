#include "Wrapper.h"
#include "MemoryPool.h"

struct State {
	KeyChanges *bindings;
	EventManager* manager;
	AchievementManager *achivements;
	ScoreManager* score;
	DialogueManager* dialogue;
	MemoryPool<Achievement, 64>   acheivementPool;
	MemoryPool<ScoreEvent, 128>   scorePool;
	MemoryPool<DialogueEvent, 64> dialoguePool;
	MemoryPool<Item, 64>          itemPool;
};

State* currentState;
void Init() {
	currentState = new State();
	currentState->bindings = new KeyChanges();
	currentState->manager = new EventManager();
	currentState->score = new ScoreManager(currentState->manager);
	currentState->achivements = new AchievementManager(currentState->manager);
	currentState->dialogue = new DialogueManager(currentState->manager);
}

void Cleanup() {
	delete currentState->bindings;
	delete currentState->achivements;
	delete currentState->manager;
	delete currentState->dialogue;
	delete currentState->score;
	delete currentState;
}


 void ChangeKeyBinding(const char* keyToChange, int value)
 {
	 currentState->bindings->ChangeKey((std::string)keyToChange, value);
 }

 void ResetKey(const char * key)
 {
	 currentState->bindings->ResetKey((std::string)key);
 }

int GetKey(const char* key)
 {
	return currentState->bindings->GetKey((std::string)key);
 }

 void ResetAllKeyBindings()
{
	 currentState->bindings->ResetAllKeys();
}

 int GetDefaultKeyValue(const char * value)
 {
	 return currentState->bindings->GetDefaultKeyValue(value);
; }

 void Undo()
 {
	 currentState->bindings->Undo();
 }

 void Redo()
 {
	 currentState->bindings->Redo();
 }

char * GetKeyName(int pos)
 {
	return currentState->bindings->GetKeyName(pos);
 }

int GetTotalAmountOfKey()
{
	return  currentState->bindings->GetTotalKeys();
}

Achievement * CreateAchievement(const char * description, int type)
{
	Achievement* myAchivement = currentState->acheivementPool.AllocInit((std::string)description, (AchivementType)type);
	return myAchivement;
}

Achievement * CreateAchievementAmount(const char * description, int type, int amount)
{
	 Achievement* myAchivement = currentState->acheivementPool.AllocInit((std::string)description, (AchivementType)type, amount);
	 return myAchivement;
}
bool isAchivementUnlocked(Achievement * achv)
 {
	 return achv->isAchivementUnlocked();
 }

 char* GetAchivementInfo(Achievement * achivement)
 {
	return achivement->GetAchivementInfo();
 }

 void DeleteAcheivement(Achievement* achievement) {
	 currentState->acheivementPool.Free(achievement);
 }

 void SendEvent(void * data, int type)
{
	 Event myEvent;
	 myEvent.myType = (Type)type;
	 myEvent.data = data;
	 currentState->manager->SetState(myEvent);
}

 ScoreEvent * AddScore(int score) {
	 ScoreEvent* sEvent = currentState->scorePool.Allocate();
	 sEvent->score = score;
	 return sEvent;
 }

 int GetScore() {
	 return currentState->score->GetScore();
 }

 void DeleteScore(ScoreEvent* score) {
	 currentState->scorePool.Free(score);
 }

 Item* AddItem(const char* name, int amount) {
	 return currentState->itemPool.AllocInit((std::string)name, amount);
 }

 char* GetItemName(const Item* item) {
	 return item->GetItemName();
 }

 void SetItemAmount(Item* item, int amount) {
	 item->setItemAmount(amount);
 }

 int GetItemAmount(const Item* item) {
	 return item->GetItemAmount();
 }

 void DeleteItem(Item* item) {
	 currentState->itemPool.Free(item);
 }

char * GetDialogue()
 {
	return currentState->dialogue->GetDialougeLines();
 }

DialogueEvent * CreateDialogue(const char* filename)
{
	DialogueEvent* myEvent = currentState->dialoguePool.Allocate();
	myEvent->filename = (std::string)filename;
	return myEvent;

}

void DeleteDialogue(DialogueEvent * item) {
	currentState->dialoguePool.Free(item);
}
